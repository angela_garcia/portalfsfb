package com.portalfsfb.portal.entity;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "userprovider")
public class UserProvider implements Serializable {

	private static final long serialVersionUID = -6186639174430557795L;
	@Id
	@GeneratedValue(generator = "native_generator")
	private int userproviderId;
	@Column(name = "userproviderNit")
	String userProviderNit;
	@Column(name = "userproviderName")
	String userProviderName;
	@Column(name = "UserproviderLastname")
	String UserProviderLastname;
	@Column(name = "userproviderEmail")
	String userProviderEmail;
	@Column(name = "userproviderEmailapprover")
	String userProviderEmailApprover;
	@Column(name = "userproviderNamecompany")
	String userProviderNameCompany;
	@Column(name = "userproviderData")
	String userProviderData;
	@Column(name = "userproviderDatecreated")
	String userProviderDateCreated;
	@Column(name = "providerTypeid")
	int providertypeId;
	@Column(name = "accountingUserid")
	int accountinguserId;
	public UserProvider(int userproviderId, String userProviderNit, String userProviderName,
			String userProviderLastname, String userProviderEmail, String userProviderEmailApprover,
			String userProviderNameCompany, String userProviderData, String userProviderDateCreated, int providertypeId,
			int accountinguserId) {
		super();
		this.userproviderId = userproviderId;
		this.userProviderNit = userProviderNit;
		this.userProviderName = userProviderName;
		UserProviderLastname = userProviderLastname;
		this.userProviderEmail = userProviderEmail;
		this.userProviderEmailApprover = userProviderEmailApprover;
		this.userProviderNameCompany = userProviderNameCompany;
		this.userProviderData = userProviderData;
		this.userProviderDateCreated = userProviderDateCreated;
		this.providertypeId = providertypeId;
		this.accountinguserId = accountinguserId;
	}
	public  UserProvider() {}
	
	public int getUserproviderId() {
		return userproviderId;
	}
	public void setUserproviderId(int userproviderId) {
		this.userproviderId = userproviderId;
	}
	public String getUserProviderNit() {
		return userProviderNit;
	}
	public void setUserProviderNit(String userProviderNit) {
		this.userProviderNit = userProviderNit;
	}
	public String getUserProviderName() {
		return userProviderName;
	}
	public void setUserProviderName(String userProviderName) {
		this.userProviderName = userProviderName;
	}
	public String getUserProviderLastname() {
		return UserProviderLastname;
	}
	public void setUserProviderLastname(String userProviderLastname) {
		UserProviderLastname = userProviderLastname;
	}
	public String getUserProviderEmail() {
		return userProviderEmail;
	}
	public void setUserProviderEmail(String userProviderEmail) {
		this.userProviderEmail = userProviderEmail;
	}
	public String getUserProviderEmailApprover() {
		return userProviderEmailApprover;
	}
	public void setUserProviderEmailApprover(String userProviderEmailApprover) {
		this.userProviderEmailApprover = userProviderEmailApprover;
	}
	public String getUserProviderNameCompany() {
		return userProviderNameCompany;
	}
	public void setUserProviderNameCompany(String userProviderNameCompany) {
		this.userProviderNameCompany = userProviderNameCompany;
	}
	public String getUserProviderData() {
		return userProviderData;
	}
	public void setUserProviderData(String userProviderData) {
		this.userProviderData = userProviderData;
	}
	public String getUserProviderDateCreated() {
		return userProviderDateCreated;
	}
	public void setUserProviderDateCreated(String userProviderDateCreated) {
		this.userProviderDateCreated = userProviderDateCreated;
	}
	public int getProvidertypeId() {
		return providertypeId;
	}
	public void setProvidertypeId(int providertypeId) {
		this.providertypeId = providertypeId;
	}
	public int getAccountinguserId() {
		return accountinguserId;
	}
	public void setAccountinguserId(int accountinguserId) {
		this.accountinguserId = accountinguserId;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}


}
