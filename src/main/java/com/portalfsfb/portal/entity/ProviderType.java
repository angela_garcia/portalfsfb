package com.portalfsfb.portal.entity;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "providertype")
public class ProviderType implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 7027865924625330794L;
	@Id
	@GenericGenerator(name = "native_generator", strategy = "native")
	@GeneratedValue(generator = "native_generator")
	private int providertypeid;
	@Column(name = "providertypename")
	String providertypename;
	
	public ProviderType(int providertypeid, String providertypename) {
		super();
		this.providertypeid = providertypeid;
		this.providertypename = providertypename;
	}

	public int getProvidertypeid() {
		return providertypeid;
	}

	public void setProvidertypeid(int providertypeid) {
		this.providertypeid = providertypeid;
	}

	public String getProvidertypename() {
		return providertypename;
	}

	public void setProvidertypename(String providertypename) {
		this.providertypename = providertypename;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
