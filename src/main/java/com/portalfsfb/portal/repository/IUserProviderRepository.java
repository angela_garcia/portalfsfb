package com.portalfsfb.portal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.portalfsfb.portal.entity.UserProvider;
import java.util.List;

@Repository
public interface IUserProviderRepository extends JpaRepository<UserProvider, Integer> {
	List<UserProvider> findByuserProviderNit(String userproviderNit);
	
}
