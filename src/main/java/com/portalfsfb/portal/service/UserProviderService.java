package com.portalfsfb.portal.service;

import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;

import com.portalfsfb.portal.entity.UserProvider;

@Service
public interface UserProviderService  {
	
	public List<UserProvider> findAll();
				
	public UserProvider saveUser(UserProvider user);
 
	public Optional<UserProvider> findById(Integer userproviderId);
		
	public List<UserProvider> findByuserProviderNit(String userproviderNit);
}
