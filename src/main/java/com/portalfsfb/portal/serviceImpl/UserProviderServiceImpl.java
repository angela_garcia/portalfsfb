package com.portalfsfb.portal.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.portalfsfb.portal.entity.UserProvider;
import com.portalfsfb.portal.repository.IUserProviderRepository;
import com.portalfsfb.portal.service.UserProviderService;

@Service
@Transactional(readOnly = true)
public class UserProviderServiceImpl implements UserProviderService {
 	@Autowired
	IUserProviderRepository repository;
 	
 	List<UserProvider> user;

	@Override
	@Transactional
	public UserProvider saveUser(UserProvider user) {
		return repository.save(user);
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<UserProvider> findById(Integer userproviderId) {
		return repository.findById(userproviderId);
	}

	@Override
	@Transactional(readOnly = true)
	public List<UserProvider> findAll() {
		return repository.findAll();
	}

	@Override
	public List<UserProvider> findByuserProviderNit(String userproviderNit) {
		return repository.findByuserProviderNit(userproviderNit);
	}

	
}
