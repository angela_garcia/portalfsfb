package com.portalfsfb.portal.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.portalfsfb.portal.entity.UserProvider;
import com.portalfsfb.portal.service.UserProviderService;

@RestController
@RequestMapping("/users")
public class UserProviderController {

	@Autowired
	private UserProviderService service;

	// Create userProvider where identification does not exist
	@PostMapping
	public ResponseEntity<?> create(@RequestBody UserProvider userprovider) {
		List<UserProvider> oUser = service.findByuserProviderNit(userprovider.getUserProviderNit());
		if (!oUser.isEmpty()) {
			return ResponseEntity.status(HttpStatus.FOUND)
					.body("Ya se encuentra un usuario con esta identificación");
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(service.saveUser(userprovider));
	}
/*
 * 	// Read usersProviders by id
	@GetMapping("/{id}")
	public ResponseEntity<?> readUsers(@PathVariable(value = "id") Integer userprovidernit) {
		Optional<UserProvider> oUser = service.findById(userprovidernit);
		if (!oUser.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(oUser);
	}

	// Read all usersProviders
	@GetMapping("/all")
	public ResponseEntity<?> getAll() {
		List<UserProvider> oUser = service.findAll();
		if (oUser.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(oUser);
	}

	// Read usersProviders by id
	@GetMapping("/nit/{userProviderNit}")
	public ResponseEntity<?> readUsersNit(@PathVariable(value = "userProviderNit") String userprovidernit) {
		List<UserProvider> oUser = service.findByuserProviderNit(userprovidernit);
		if (oUser.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(oUser);
	}
 */

}
